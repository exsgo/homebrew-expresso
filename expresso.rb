# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class Expresso < Formula
  desc "The compiler for the Expresso language"
  homepage "https://www.expresso-lang.org"
  url "https://gitlab.com/exsgo/expresso-lang/-/archive/1.1.12/expresso-lang-1.1.12.tar.gz"
  sha256 "0ce0a3cc1e0b1a996c49f30f45dd60034a27a895868638ba54ac6a76a86df80b"
  depends_on "mono"
  depends_on "nuget" => :build

  def install
    # ENV.deparallelize  # if your formula fails when building in parallel
    # Remove unrecognized options if warned by configure
    system "make"
    system "make", "install", "PREFIX=#{prefix}"
    (prefix/"bin/exsc").write <<~EOS
      #! /bin/sh
      exec mono #{prefix}/lib/exsc/exsc.exe "$@"
    EOS
  end

  def caveats
    msg = <<~EOF
    Next you must run "echo export MONO_PATH=#{prefix}/lib/exsc >> ~/.bashrc" to execute your Expresso programs.
    We'll find a way to automate it, but thanks for doing that!
    And run "source ~/.bashrc" or open a new terminal to finish your installation.

EOF
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test expresso-lang`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
